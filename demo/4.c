#include "cfg.h"

#include <util/delay.h>

int
main(void)
{
	log_init();
	k0135_init();

	u8 lvl;
	while (TRUE) {
		lvl = k0135_read();

		log_str("level: ");
		log_u8(lvl);
		log_str("\r\n");

		_delay_ms(500);
	}
	return 0;
}
