#ifndef __Z0_AVR_KY037_H__
#define __Z0_AVR_KY037_H__

#include "types.h"

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_KY037_SHORT_NAMES)
	#ifdef KY037_DDR_D
		#define Z0_AVR_KY037_DDR_D KY037_DDR_D
	#endif
	#ifdef KY037_PIN_D
		#define Z0_AVR_KY037_PIN_D KY037_PIN_D
	#endif
	#ifdef KY037_IDX_D
		#define Z0_AVR_KY037_IDX_D KY037_IDX_D
	#endif
	#ifdef KY037_DDR_A
		#define Z0_AVR_KY037_DDR_A KY037_DDR_A
	#endif
	#ifdef KY037_IDX_A
		#define Z0_AVR_KY037_IDX_A KY037_IDX_A
	#endif
	#ifdef KY037_DIGITAL
		#define Z0_AVR_KY037_DIGITAL KY037_DIGITAL
	#endif
#endif

#ifdef Z0_AVR_KY037_DIGITAL
	#ifndef Z0_AVR_KY037_DDR_D
		#error "KY-037 digital DDR register is not defined"
	#endif
	#ifndef Z0_AVR_KY037_PIN_D
		#error "KY-037 digital PIN register is not defined"
	#endif
	#ifndef Z0_AVR_KY037_IDX_D
		#error "KY-037 digital IDX register is not defined"
	#endif
#else
	#ifndef Z0_AVR_KY037_DDR_A
		#error "KY-037 analog DDR register is not defined"
	#endif
	#ifndef Z0_AVR_KY037_IDX_A
		#error "KY-037 analog IDX register is not defined"
	#endif
#endif

#ifdef Z0_AVR_KY037_DIGITAL

void
z0_avr_ky037_init_d(void);

u8
z0_avr_ky037_read_d(void);

#else

void
z0_avr_ky037_init_a(void);

u8
z0_avr_ky037_read_a(void);

#endif

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_KY037_SHORT_NAMES)
	#define ky037_init_d z0_avr_ky037_init_d
	#define ky037_read_d z0_avr_ky037_read_d
	#define ky037_init_a z0_avr_ky037_init_a
	#define ky037_read_a z0_avr_ky037_read_a
#endif

#endif
