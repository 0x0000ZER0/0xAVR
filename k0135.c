#include "cfg.h"

void
z0_avr_k0135_init(void)
{
	K0135_DDR &= ~(1 << K0135_IDX); 

	ADMUX  =  (1 << REFS0) | (1 << ADLAR) | K0135_IDX;
	ADCSRA =  (1 << ADEN) |  (1 << ADPS1);
}

u8
z0_avr_k0135_read(void)
{
	ADCSRA |= (1 << ADSC);
	while (ADCSRA & (1 << ADSC));

	return ADCH;
}
