#include "cfg.h"

#include <util/delay.h>

u8
z0_avr_dht11_init(void)
{
	DHT11_DDR |= (1 << DHT11_IDX);
	DHT11_PORT |= (1 << DHT11_IDX);
	
	DHT11_PORT &= ~(1 << DHT11_IDX);
	_delay_ms(18);
	                        
	DHT11_PORT |= (1 << DHT11_IDX); 
	DHT11_DDR  &= ~(1 << DHT11_IDX);     
	_delay_us(40);               
	
	if (DHT11_PIN & (1 << DHT11_IDX))
	        return Z0_AVR_DHT11_INIT_ERR;
	_delay_us(80);
	
	if (!(DHT11_PIN & (1 << DHT11_IDX)))
	        return Z0_AVR_DHT11_INIT_ERR;
	_delay_us(80);
	
	return Z0_AVR_DHT11_INIT_OK;
}

static u8
dht11_read_bit(void)
{
        while (!(DHT11_PIN & (1 << DHT11_IDX)));

        _delay_us(28);
        if (DHT11_PIN & (1 << DHT11_IDX)) {
                _delay_us(42);
                return 1;
        } 

        return 0;
}

u8
z0_avr_dht11_read(void)
{
	u8 r;
	r = 0;
	
	u8 b;
	for (u8 i = 8; i > 0; --i) {
	        b = dht11_read_bit();
	        r |= (b << (i - 1));
	}
	
	return r;
}
