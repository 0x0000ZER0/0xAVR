#ifndef __Z0_AVR_LOG_H__
#define __Z0_AVR_LOG_H__

#include "types.h"

#if defined(Z0_AVR_LOG_SHORT_NAMES) || defined(Z0_AVR_SHORT_NAMES)
	#ifdef BAUD_RATE
		#define Z0_AVR_BAUD_RATE BAUD_RATE
	#endif
#endif

#ifndef Z0_AVR_BAUD_RATE
	#error "Baud rate is not defined"
#endif

#ifndef F_CPU
	#error "The cpu frequency is not defined"
#endif

#define Z0_AVR_BAUD_SCALE (F_CPU / 16 / Z0_AVR_BAUD_RATE - 1)

void
z0_avr_log_init(void);

void
z0_avr_log_char(char);

void
z0_avr_log_str(const char*);

void
z0_avr_log_u8(u8);

void
z0_avr_log_seq(u8*, u8);

#if defined(Z0_AVR_LOG_SHORT_NAMES) || defined(Z0_AVR_SHORT_NAMES)
	#define log_init 	z0_avr_log_init
	#define log_char 	z0_avr_log_char
	#define log_str 	z0_avr_log_str
	#define log_u8 		z0_avr_log_u8
	#define log_seq 	z0_avr_log_seq

	#define BAUD_SCALE 	Z0_AVR_BAUD_SCALE
#endif

#endif
