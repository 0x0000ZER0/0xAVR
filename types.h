#ifndef __Z0_AVR_TYPES_H__
#define __Z0_AVR_TYPES_H__

typedef signed 	 char s8;
typedef unsigned char u8;
typedef signed 	 int  s16;
typedef unsigned int  u16;

#define TRUE  1
#define FALSE 0

#endif
