#ifndef __Z0_AVR_CFG_H__
#define __Z0_AVR_CFG_H__

// -------- AVR --------
#include <avr/io.h>

#define Z0_AVR_SHORT_NAMES

#define BAUD_RATE 9600
#include "log.h"

#define DHT11_DDR  DDRB
#define DHT11_PORT PORTB
#define DHT11_PIN  PINB
#define DHT11_IDX  5
#include "dht11.h"

#ifdef KY037_DIGITAL
#define KY037_DDR_D DDRB
#define KY037_PIN_D PINB
#define KY037_IDX_D 5
#else 
#define KY037_DDR_A DDRC
#define KY037_IDX_A 5
#endif
#include "ky037.h"

#define K0135_DDR DDRC
#define K0135_IDX 5
#include "k0135.h"

#endif
