#ifndef __Z0_AVR_DHT11_H__
#define __Z0_AVR_DHT11_H__

#include "types.h"

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_DHT11_SHORT_NAMES)
	#ifdef DHT11_DDR
		#define Z0_AVR_DHT11_DDR DHT11_DDR
	#endif
	#ifdef DHT11_PORT
		#define Z0_AVR_DHT11_PORT DHT11_PORT
	#endif
	#ifdef DHT11_PIN
		#define Z0_AVR_DHT11_PIN DHT11_PIN
	#endif
	#ifdef DHT11_IDX
		#define Z0_AVR_DHT11_IDX DHT11_IDX
	#endif
#endif

#ifndef Z0_AVR_DHT11_DDR
	#error "DHT11 DDR register is not defined"
#endif

#ifndef Z0_AVR_DHT11_PORT
	#error "DHT11 PORT register is not defined"
#endif

#ifndef Z0_AVR_DHT11_PIN
	#error "DHT11 PIN register is not defined"
#endif

#ifndef Z0_AVR_DHT11_IDX
	#error "DHT11 IDX register is not defined"
#endif

enum {
	Z0_AVR_DHT11_INIT_OK = 0,
	Z0_AVR_DHT11_INIT_ERR
};

u8
z0_avr_dht11_init(void);

u8
z0_avr_dht11_read(void);

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_DHT11_SHORT_NAMES)
	#define dht11_init 	z0_avr_dht11_init
	#define dht11_read 	z0_avr_dht11_read

	#define DHT11_INIT_OK 	Z0_AVR_DHT11_INIT_OK
	#define DHT11_INIT_ERR 	Z0_AVR_DHT11_INIT_ERR
#endif

#endif
