#ifndef __Z0_AVR_K0135_H__
#define __Z0_AVR_K0135_H__

#include "types.h"

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_K0135_SHORT_NAMES)
	#ifdef K0135_DDR
		#define Z0_AVR_K0135_DDR K0135_DDR
	#endif
	#ifdef K0135_IDX
		#define Z0_AVR_K0135_IDX K0135_IDX
	#endif
#endif

#ifndef K0135_DDR
	#error "K0135 DDR register is not defined"
#endif
#ifndef K0135_IDX
	#error "K0135 IDX register is not defined"
#endif

void
z0_avr_k0135_init(void);

u8
z0_avr_k0135_read(void);

#if defined(Z0_AVR_SHORT_NAMES) || defined(Z0_AVR_K0135_SHORT_NAMES)
	#define k0135_init z0_avr_k0135_init
	#define k0135_read z0_avr_k0135_read
#endif

#endif
