#include "cfg.h"

void
z0_avr_log_init(void)
{
	UBRR0H = Z0_AVR_BAUD_SCALE >> 8;
	UBRR0L = Z0_AVR_BAUD_SCALE;
	UCSR0B = (1 << RXEN0)  | (1 << TXEN0);
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);
}

void
z0_avr_log_char(char byte)
{
        while (!(UCSR0A & (1 << UDRE0)));

        UDR0 = byte;
}

void
z0_avr_log_str(const char *str)
{
	while (*str) {
		z0_avr_log_char(*str);

		++str;
	}
}

void
z0_avr_log_seq(u8 *seq, u8 len)
{
        for (u8 i = 0; i < len; ++i) {
                z0_avr_log_char(seq[i]);
        }
}

void
z0_avr_log_u8(u8 n)
{
	char buf[2];
	buf[1] = (n % 10) | 0x30;
	n /= 10;
	buf[0] = (n % 10) | 0x30;
	n /= 10;

        while (!(UCSR0A & (1 << UDRE0)));
        UDR0 = (n % 10) | 0x30;

        while (!(UCSR0A & (1 << UDRE0)));
	UDR0 = buf[0];

        while (!(UCSR0A & (1 << UDRE0)));
	UDR0 = buf[1];
}
