#include "cfg.h"

int
main(void)
{
	log_init();
	log_u8('c');
	log_str(" string ");

	const char lit[] = "sequence";
	log_seq((u8*)lit, sizeof (lit) - 1);

	while (TRUE);
	return 0;
}
