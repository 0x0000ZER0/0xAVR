#include "cfg.h"

int
main(void)
{
	log_init();

	u8 hum_int;
	u8 hum_dec;
	u8 tmp_int;
	u8 tmp_dec;
	u8 chk_sum;
	while (TRUE) {
		if (dht11_init() != DHT11_INIT_OK)
			continue;

		hum_int = dht11_read();
		hum_dec = dht11_read();
		tmp_int = dht11_read();
		tmp_dec = dht11_read();
		chk_sum = dht11_read();

		log_str("HUM: ");
		log_u8(hum_int);
		log_char('.');
		log_u8(hum_dec);
		log_str("\r\n");

		log_str("TMP: ");
		log_u8(tmp_int);
		log_char('.');
		log_u8(tmp_dec);
		log_str("\r\n");

		log_str("CHK: ");
		log_u8(chk_sum);
		log_char('=');
		log_u8(hum_int + hum_dec + tmp_int + tmp_dec);
		log_str("\r\n");
	}
	return 0;
}
