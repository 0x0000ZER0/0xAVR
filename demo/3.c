#include "cfg.h"

#include <util/delay.h>

int
main(void)
{
	log_init();

#ifdef KY037_DIGITAL
	ky037_init_d();
#else
	ky037_init_a();
#endif

	u8 val;
	while (TRUE) {
#ifdef KY037_DIGITAL
		val = ky037_read_d(); 
		if (val)
			log_str("reached\r\n");
		else
			log_str("...\r\n");
#else
		val = ky037_read_a();
		log_u8(val);
		log_str("\r\n");
#endif

		_delay_ms(500);
	}
	return 0;
}
