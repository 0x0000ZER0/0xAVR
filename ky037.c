#include "cfg.h"

#ifdef KY037_DIGITAL

void
z0_avr_ky037_init_d(void)
{
	KY037_DDR_D &= ~(1 << KY037_IDX_D);
}

u8
z0_avr_ky037_read_d(void)
{
	return (KY037_PIN_D & (1 << KY037_IDX_D));
}

#else

void
z0_avr_ky037_init_a(void)
{
	KY037_DDR_A &= ~(1 << KY037_IDX_A); 

	ADMUX  =  (1 << REFS0) | (1 << ADLAR) | KY037_IDX_A;
	ADCSRA =  (1 << ADEN) |  (1 << ADPS1);
}

u8
z0_avr_ky037_read_a(void)
{
	ADCSRA |= (1 << ADSC);
	while (ADCSRA & (1 << ADSC));

	return ADCH;
}

#endif
